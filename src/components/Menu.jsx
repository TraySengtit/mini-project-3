import React from 'react';
import { useState } from 'react';
import { Navbar, Container, Nav, Form, FormControl, Button, NavDropdown } from 'react-bootstrap';
import LocalizedStrings from 'react-localization';
import { NavLink, Link } from 'react-router-dom';
import { strings } from '../localization/localization';

function NavMenu() {
	
	const [language, setLanguage] =	useState ([
		{
			title: "English",
			key: "en"
		},
		{
			title: "Khmer",
			key: "km"
		}
	]) 
	const [update, setUpdate] = useState({})
	const change = (lang) => {
		strings.setLanguage(lang)
		setUpdate({})
	}

	return (
		<Navbar bg='primary' variant="dark">
			<Container>
				<Navbar.Brand as={NavLink} to='/'>
					AMS Redux
				</Navbar.Brand>
				<Navbar.Toggle aria-controls='basic-navbar-nav' />
				<Navbar.Collapse id='basic-navbar-nav'>
					<Nav className='me-auto'>
						<Nav.Link as={NavLink} to="/">{strings.home}</Nav.Link>
						<Nav.Link as={NavLink} to="/article">{strings.article}</Nav.Link>
						<Nav.Link as={NavLink} to="/author">{strings.author}</Nav.Link>
						<Nav.Link as={NavLink} to="/category">{strings.category}</Nav.Link>
						<NavDropdown title="Language" id="basic-nav-dropdown">
							{
								language.map((lang, index) => 
									<NavDropdown.Item key={index} onClick={()=>change(lang.key)}>
										{lang.title}
									</NavDropdown.Item>
								)
							}
						</NavDropdown>
					</Nav>
					<Form className='d-flex'>
						<FormControl type='search' placeholder='Search'  className="mr-2" aria-label='Search' />
						<Button variant='outline-light'>{strings.search}</Button>
					</Form>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
}

export default NavMenu;
