import React from "react";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Menu from './components/Menu';
import Article from './pages/Article';
import Author from './pages/Author';
import Home from './pages/Home';

function App() {
	return (
		<Router>
			<Menu />
			<Switch>
				<Route exact path='/' component={Home} />
				<Route path='/article' component={Article} />
				<Route path='/author' component={Author} />
			</Switch>
		</Router>
	);
}

export default App;
