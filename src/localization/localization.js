// ES6 module syntax
import LocalizedStrings from 'react-localization';

export const strings = new LocalizedStrings({
    en: {
        home: "Home",
        article: "Article",
        author: "Author",
        category: "Category",
        languages: "Language",
        search: "Search"
    },
    km: {
        home: "ទំព័រដើម",
        article: "អត្ថបទ",
        author: "អ្នកនិពន្ធ",
        category: "ប្រភេទ",
        languages: "ភាសា",
        search: "ស្វែងរក"
    }
});