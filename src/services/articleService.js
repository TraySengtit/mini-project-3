import api from '../utils/api';

export const fetchArticles = async () => {
    let response = await api.get('articles');
    return response.data.data;
};