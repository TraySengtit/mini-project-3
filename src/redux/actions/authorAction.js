import { fetchAuthors, postAuthor, deleteAuthor, updateAuthorById } from '../../services/authorService';
import { authorActionType } from './authorActionType';

export const onFetchAuthors = () => async (dispatch) => {
	let authors = await fetchAuthors();
	dispatch({
		type: authorActionType.FETCH_AUTHORS,
		payload: authors,
	});
};

export const onInsertAuthor = (newAuthor) => async (dispatch) => {
	let author = await postAuthor(newAuthor);
	dispatch({
		type: authorActionType.INSERT_AUTHOR,
		payload: author,
	});
};

export const onDeleteAuthor = (authorId) => async (dispatch) => {
	let message = await deleteAuthor(authorId);
	dispatch({
		type: authorActionType.DELETE_AUTHOR,
		payload: authorId,
	});
};

export const onUpdateAuthor = (authorId, updatedAuthor) => async (dispatch) => {
	let message = await updateAuthorById(authorId, updatedAuthor);
	dispatch({
		type: authorActionType.UPDATE_AUTHOR,
		payload: { authorId, updatedAuthor },
	});
};
