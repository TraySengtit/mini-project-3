import api from "../../utils/api";


export const fetchArtical = () => async dp => {
    let response = await api.get('articles');
    return dp({
        type: "FETCH_ARTICLE",
        payload: response.data.data
    })

};