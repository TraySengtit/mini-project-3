import { combineReducers } from 'redux';
import authorReducer from './authorReducer';
import articleReducer from './articleReducer'

const rootReducer = combineReducers({
    authorReducer,
    articleReducer
});

export default rootReducer;
