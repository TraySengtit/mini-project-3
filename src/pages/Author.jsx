import React, { useEffect, useState } from 'react';
import { Container, Table, Button, Col, Row, Form } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { onFetchAuthors, onInsertAuthor, onDeleteAuthor, onUpdateAuthor } from '../redux/actions/authorAction';
import { uploadImage } from '../services/authorService';

function Author() {
	const [authorName, setAuthorName] = useState('');
	const [email, setEmail] = useState('');
	const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png');
	const [imageFile, setImageFile] = useState(null);
	const [selectedId, setSelectedId] = useState('');

	const authors = useSelector(state => state.authorReducer.authors);
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(onFetchAuthors());
	}, []);
	let onAddOrUpdate = async (e) => {
		e.preventDefault();
		let newAuthor = {
			name: authorName,
			email,
		};
		if (imageFile) {
			let url = await uploadImage(imageFile);
			newAuthor.image = url;
		} else {
			newAuthor.image = imageURL;
		}
		if (selectedId) {
			dispatch(onUpdateAuthor(selectedId, newAuthor));
		} else {
			dispatch(onInsertAuthor(newAuthor));
		}
		resetForm();
	};

	const onDelete = (id) => {
		// if selected author was deleted, we reset the form
		if (id === selectedId) resetForm();

		dispatch(onDeleteAuthor(id));
	};

	let resetForm = () => {
		setAuthorName('');
		setEmail('');
		setImageURL('https://designshack.net/wp-content/uploads/placeholder-image.png');
		setImageFile(null);
		setSelectedId('');
	};

	return (
		<Container>
			<h1 className='my-3'>Add Author</h1>
			<Row className="my-4">
				<Col md={8}>
					<Form>
						<Form.Group controlId='title'>
							<Form.Label>Author's Name</Form.Label>
							<Form.Control
								type='text'
								placeholder="Author's Name"
								value={authorName}
								onChange={(e) => setAuthorName(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>

						<Form.Group className="mt-2" controlId='email'>
							<Form.Label>Email</Form.Label>
							<Form.Control
								type='text'
								placeholder='Email'
								value={email}
								onChange={(e) => setEmail(e.target.value)}
							/>
							<Form.Text className='text-muted'></Form.Text>
						</Form.Group>

						<Button className="mt-2" variant='primary' onClick={onAddOrUpdate}>
							{selectedId ? 'Update' : 'Add'}
						</Button>
					</Form>
				</Col>
				<Col md={4}>
					<img className='w-100' src={imageURL} />
					<Form>
						<Form.Group>
							<Form.File
								id='img'
								label='Choose Image'
								onChange={(e) => {
									let url = URL.createObjectURL(e.target.files[0]);
									setImageFile(e.target.files[0]);
									setImageURL(url);
								}}
							/>
						</Form.Group>
					</Form>
				</Col>
			</Row>

			<Table striped bordered hover>
				<thead>
					<tr style={{textAlign: 'center'}}>
						<th>#</th>
						<th>Name</th>
						<th>Email</th>
						<th>Image</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{authors.map((author, index) => (
						<tr key={author._id} >
							<td style={{width: '50px'}}>{author._id}</td>
							<td>{author.name}</td>
							<td>{author.email}</td>
							<td style={{width: '30px'}}>
								<img style={{ objectFit: 'cover', height: '80px' }} src={author.image} alt='' />
							</td>
							<td>
								<Button
									size='sm'
									variant='warning'
									onClick={() => {
										setSelectedId(author._id);
										setAuthorName(author.name);
										setEmail(author.email);
										setImageURL(author.image);
									}}
								>
									Edit
								</Button>{' '}
								<Button size='sm' variant='danger' onClick={() => onDelete(author._id)}>
									Delete
								</Button>
							</td>
						</tr>
					))}
				</tbody>
			</Table>
		</Container>
	);
}

export default Author;
