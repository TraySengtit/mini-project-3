import React,{useState,useEffect} from "react";
import { Col, Container, Row, Form, Button } from "react-bootstrap";
import { onFetchAuthors } from "../redux/actions/authorAction";
import { useDispatch, useSelector } from 'react-redux';


function Article() {

    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [imageURL, setImageURL] = useState('https://designshack.net/wp-content/uploads/placeholder-image.png')
    const [imageFile, setImageFile] = useState(null)
    const [categoryId, setCategoryId] = useState(null)

    const [category, setCategory] = useState([])

    const authors = useSelector(state => state.authorReducer.authors);
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(onFetchAuthors);
	}, []);
    return (
    <Container>
        <h1 className="my-2">Add Article</h1>
        <Row>
            <Col md={8}>
            <Form>
                <Form.Group controlId="title">
                <Form.Label>Title</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Title" 
                    value={title}
                    onChange={(e)=>setTitle(e.target.value)}
                    />
                <Form.Text className="text-muted">
                </Form.Text>
                </Form.Group>

                <Form.Group controlId="description">
                <Form.Label>Category</Form.Label>
                <Form.Control 
                    as='select' 
                    aria-label="Choosse Category"
                    onChange={(e)=>setCategoryId(e.target.value)}
                    >
                    {
                    authors.map(author=>
                        <option key={author._id} value={author._id} selected={author._id}>{author.name}</option>
                    )
                    }
                </Form.Control>
                
                </Form.Group>

                <Form.Group controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control 
                    as="textarea" 
                    rows={4} 
                    placeholder="Description" 
                    value={description}
                    onChange={(e)=>setDescription(e.target.value)}
                />
                
            </Form.Group>
            <Button 
                variant="primary" 
                type="submit"
                
            >
                Submit
                </Button>
            </Form>
        </Col>
        <Col md={4}>
            <img className="w-100" src={imageURL}/>
            <Form>
            <Form.Group>
                <Form.File 
                    id="img" 
                    label="Choose Image" 
                    onChange={(e)=>{
                        let url = URL.createObjectURL(e.target.files[0])
                        setImageFile(e.target.files[0])
                        setImageURL(url)
                    }}
                    />
            </Form.Group>
            </Form>
        </Col>
        </Row>
        </Container>
    );
}

export default Article;
